'use strict';
import React, {
  Component,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#48BBEC'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center',
    color: '#fff'
  }
});


export class Dashboard extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.title}>Found a GitHub user.</Text>
        <Text>{JSON.stringify(this.props.userInfo)}</Text>
      </View>
    );
  }
}
